import cv2

face_cascade = cv2.CascadeClassifier("haarcascade_fullbody.xml")

cam1 = cv2.VideoCapture(0)
cam2 = cv2.VideoCapture(1)
while True:
    _, img = cam1.read()
    _, img2 = cam2.read()
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY) 
    bodies = face_cascade.detectMultiScale(
        gray, 
        scaleFactor=1.1,
        minNeighbors=3, 
        minSize=(30, 30), 
        maxSize=(400, 400))
    gray2 = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY) 
    bodies2 = face_cascade.detectMultiScale(
        gray2, 
        scaleFactor=1.1,
        minNeighbors=3, 
        minSize=(30, 30), 
        maxSize=(400, 400))
    
    
    for (x, y, w, h) in bodies:
        cv2.rectangle(img, (x, y), (x+w, y+h), (255, 0, 0), 2)

    for (x, y, w, h) in bodies2:
        cv2.rectangle(img2, (x, y), (x+w, y+h), (255, 0, 0), 2)

    cv2.imshow('img', img)
    cv2.imshow('img2', img2)


    k = cv2.waitKey(30)
    if k == 27:
        break

cam1.release()
#cam2.release()