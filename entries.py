from tkinter import *

ventana = Tk()
ventana.title("Entrada")
ventana.tk.call('wm', 'geometry', ventana._w, "400x300+100+100")

IP = IntVar()
Subred = IntVar()
Puerta_de_enlace = IntVar()

etiqueta1 =  Label(ventana, text="IP address")
etiqueta1.place(x=10, y=10)
entrada1= Entry(textvariable= IP)
entrada1.place(x=170, y=10)

etiqueta2 =  Label(ventana, text="Mascara de subred")
etiqueta2.place(x=10, y=40)
entrada2= Entry(textvariable= Subred)
entrada2.place(x=170, y=40)

etiqueta3 =  Label(ventana, text="Puerta de enlace")
etiqueta3.place(x=10, y=80)
entrada3= Entry(textvariable= Puerta_de_enlace)
entrada3.place(x=170, y=80)

ventana.mainloop()