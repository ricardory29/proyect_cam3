from tkinter import *
import tkinter
import time
import os
import subprocess

ventana = tkinter.Tk()
ventana.tk.call('wm', 'geometry', ventana._w, "800x600+100+100")
etiqueta = tkinter.Label(ventana, text = "Sistema de videovigilancia Equipate", background="orange")
etiqueta.place(x=370, y=0)

menuBar = Menu(ventana)

ventana.config(menu=menuBar)

archivoMenu = Menu(menuBar)

archivoMenu.add_command(label="Nuevo")
archivoMenu.add_command(label="Abrir")
archivoMenu.add_command(label="Guardar")
archivoMenu.add_command(label="Cerrar")
archivoMenu.add_separator()
archivoMenu.add_command(label="Salir")


editMenu = Menu(menuBar)

editMenu.add_command(label="Recortar")
editMenu.add_command(label="Copiar")
editMenu.add_command(label="Pegar")

menuBar.add_cascade(label="Archivo", menu=archivoMenu)
menuBar.add_cascade(label="Editar", menu=editMenu)




def editar():
    label1 = Label(ventana, text= "            Ingresando al sistema...            ")
    label1.place(x=0, y=50)

def capturar_video():
    label2 = Label(ventana, text= "Capturando video en tiempo real...")
    label2.place(x=0, y=50)
    archivo_ruta = "cv_fullbody.py"
    if not os.path.exists(archivo_ruta):
        print(f'El archivo {archivo_ruta} no existe.')
    else:
        try:
        # Ejecutar el archivo como un proceso externo
            subprocess.run(['python', archivo_ruta], check=True)
        except subprocess.CalledProcessError as e:
            print(f'Error al ejecutar el archivo: {e}')

def preferencias():
    label3 = Label(ventana, text= "          Ingresando a preferencias...          ")
    label3.place(x=0, y=50)

def configuracion_red():
    label4 = Label(ventana, text= "Recuperando configuraciones de red...")
    label4.place(x=0, y=50)
    archivo_ruta = "entries.py"
    if not os.path.exists(archivo_ruta):
        print(f'El archivo {archivo_ruta} no existe.')
    else:
        try:
        # Ejecutar el archivo como un proceso externo
            subprocess.run(['python', archivo_ruta], check=True)
        except subprocess.CalledProcessError as e:
            print(f'Error al ejecutar el archivo: {e}')

boton1 = Button(ventana, text = "Minimizar", command=ventana.iconify)
boton1.place(x=720, y=0)


boton2= Button(ventana, text= "Capturar video", command=capturar_video)
boton2.place(x=50, y=0)


boton4= Button(ventana, text= "Preferencias", command=preferencias)
boton4.place(x=135, y=0)

boton5= Button(ventana, text= "Configuración de Red", command=configuracion_red)
boton5.place(x=205, y=0)

ventana.mainloop()
